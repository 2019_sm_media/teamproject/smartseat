from .models import Member, SensorLog, TestData
from rest_framework import serializers, viewsets
from django.http import HttpResponse, request


# Members_select
class MembersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Member
        fields = '__all__'
class MembersViewSet(viewsets.ModelViewSet):
    queryset = Member.objects.all()
    serializer_class = MembersSerializer


# SensorLog_select
class SensorLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = SensorLog
        fields = '__all__'
class SensorLogViewSet(viewsets.ModelViewSet):
    queryset = SensorLog.objects.all()
    serializer_class = SensorLogSerializer


# SensorLog_select
class TestLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestData
        fields = '__all__'
class TestLogViewSet(viewsets.ModelViewSet):
    queryset = TestData.objects.all()
    serializer_class = SensorLogSerializer

