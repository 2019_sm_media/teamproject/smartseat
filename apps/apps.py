from django.apps import AppConfig


class MigrationConfig(AppConfig):
    name = 'apps'
