from django.db import models


class Member(models.Model):
    mem_id = models.CharField(max_length=200, primary_key=True)
    mem_gender = models.CharField(max_length=20)
    mem_age = models.IntegerField()
    mem_job = models.CharField(max_length=100)
    mem_weight = models.CharField(max_length=100)
    mem_body = models.CharField(max_length=100)

    def __str__(self):
        return self.mem_id


class SensorLog(models.Model):
    mem_id = models.CharField(max_length=200)
    r_edge = models.CharField(max_length=100)
    l_edge = models.CharField(max_length=100)
    l_vector = models.CharField(max_length=100)
    r_sum = models.CharField(max_length=100)
    log_date = models.DateTimeField('date published')

    def __str__(self):
        return self.mem_id


class TestData(models.Model):
    mem_id = models.CharField(max_length=200)
    r_edge = models.CharField(max_length=100)
    l_edge = models.CharField(max_length=100)
    l_vector = models.CharField(max_length=100)
    r_sum = models.CharField(max_length=100)
    log_state = models.CharField(max_length=20)
    log_date = models.DateTimeField('date published')

    def __str__(self):
        return self.mem_id

# Create your models here.
