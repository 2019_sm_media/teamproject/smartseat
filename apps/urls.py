from django.urls import path
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers
from rest_framework_swagger.views import get_swagger_view
from . import views
import apps.api

app_name = 'member'


app_name = 'apps'

# Router
router = routers.DefaultRouter()
router.register('members', apps.api.MembersViewSet)
router.register('sensorLog', apps.api.SensorLogViewSet)
router.register('testLog', apps.api.TestLogViewSet)

urlpatterns = [
    # Django URL
    path('', views.index, name='index'),
    path('sign/<str:mem_id>/<str:mem_gender>/<int:mem_age>/<str:mem_job>/<str:mem_weight>/<str:mem_body>/', views.sign, name='sign'),
    path('log/<str:mem_id>/<str:r_edge>/<str:l_edge>/<str:l_vector>/<str:r_sum>', views.log, name='log'),
    path('test/<str:mem_id>/<str:r_edge>/<str:l_edge>/<str:l_vector>/<str:r_sum>/<str:log_state>', views.test, name='test'),

    # REST _ URL
    url(r'^api/doc', get_swagger_view(title='Rest API Document')),
    url(r'^api/', include((router.urls, 'members'), namespace='api')), # apps/api/member/mem_id => filter query
    url(r'^api/', include((router.urls, 'sensorLog'), namespace='api')), # apps/api/sensorLog
    url(r'^api/', include((router.urls, 'testLog'), namespace='api')),  # apps/api/testLog
]
