from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.http import Http404
from django.shortcuts import render
from .models import Member,SensorLog, TestData
from django.utils import timezone

def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")

def sign(request,mem_id, mem_gender, mem_age, mem_job, mem_weight, mem_body):

    member = Member(mem_id=mem_id, mem_gender=mem_gender, mem_age=mem_age, mem_job=mem_job, mem_weight=mem_weight, mem_body=mem_body)
    member.save()
    return HttpResponse("Success")

def log(request,mem_id, r_edge, l_edge, l_vector, r_sum):

    sensor = SensorLog(mem_id=mem_id,r_edge=r_edge,l_edge=l_edge, l_vector=l_vector, r_sum=r_sum, log_date=timezone.datetime.now());
    sensor.save()
    return HttpResponse("Log Success")

def test(request, mem_id, r_edge, l_edge, l_vector, r_sum, log_state):

    testData = TestData(mem_id=mem_id, r_edge=r_edge, l_edge=l_edge, l_vector=l_vector, r_sum=r_sum, log_state=log_state,
                       log_date=timezone.datetime.now());
    testData.save()
    return HttpResponse("Log Success")

